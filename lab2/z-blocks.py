def StrComp(s, n, i1, i2):
    eqLen = 0
    while i1 < n and i2 < n and s[i1] == s[i2]:
        i1 += 1
        i2 += 1
        eqLen += 1
    return eqLen


def prefix_z_values(s):
    n = len(s)
    zp = [0 for i in range(n)]
    l = r = 0
    zp[0] = 0
    for i in range(1, n):
        zp[i] = 0
        if i >= r:
            zp[i] = StrComp(s, n, 0, i)
            l = i
            r = l + zp[i]
        else:
            j = i - l
            if zp[j] < r - i:
                zp[i] = zp[j]
            else:
                zp[i] = r - i + StrComp(s, n, r - i, r)
                l = i
                r = l + zp[i]
    return zp


if __name__ == '__main__':
    s = ["c", "a", "c", "z", "z", "z", "c", "a", "c", "a"]
    # s = ["A", "B", "X", "A", "B", "Z", "M", "A", "B", "X", "A", 'B', "Z"]
    print(s)
    print(prefix_z_values(s))
