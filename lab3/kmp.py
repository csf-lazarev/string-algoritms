from lab1.bp_and_bs import prefix_border_array
from lab1.bp_to_bpm import bp_to_bmp


def kmp(p, t):
    bp = prefix_border_array(p)
    m = len(p)
    n = len(t)
    bpm = bp_to_bmp(bp, m)
    k = 0
    for i in range(n):
        while k and p[k] != t[i]:
            k = bpm[k - 1]

        if p[k] == t[i]:
            k = k + 1
        if k == m:
            print(f"Match from pos: {i - k + 1}")
            k = bpm[k - 1]


if __name__ == '__main__':
    text = "texttevtttexs"
    pattern = "tex"
    kmp(pattern, text)
