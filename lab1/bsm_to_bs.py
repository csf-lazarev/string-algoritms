def bms_to_bs(bsm: list, n) -> list:
    bs = [0 for _ in range(n)]
    bs[0] = bsm[0]
    bs[n - 1] = 0
    for i in range(1, n - 1):
        bs[i] = max(bs[i - 1] - 1, bsm[i])
    return bs


if __name__ == '__main__':
    bsm = [0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2, 6]
    print(bms_to_bs(bsm, len(bsm)))
