def prefix_border_array(line: str) -> list:
    n = len(line)
    bp = [0 for _ in range(n)]  # массив максимальных граней
    for i in range(1, n):
        bp_right = bp[i - 1]  # предыдущее значение массива граней
        while bp_right and (line[i] != line[bp_right]):
            # если в предыдущей позиции сущствовала грань, последний символ которой не равен окончанию
            # выбранного префикса  (пример: AAB)
            bp_right = bp[bp_right - 1]
        if line[i] == line[bp_right]:
            # если последний символ выбранного префикса равен окночнаию предыдущей грани (пример: AAА)
            bp[i] = bp_right + 1  # длина максимальной грани увеличивается
        else:
            bp[i] = 0
    return bp


def suffix_border_array(line: str) -> list:
    n = len(line)
    bs = [0 for _ in range(n)]
    for i in reversed(range(n - 2)):
        bs_left = bs[i + 1]  # индекс слева от предыдущей грани
        while bs_left and (line[i] != line[n - bs_left - 1]):
            bs_left = bs[n - bs_left]
        if line[i] == line[n - bs_left - 1]:
            bs[i] = bs_left + 1
        else:
            bs[i] = 0
    return bs


test = 'ABAABABAABAAB'
print(*prefix_border_array(test))
print(*suffix_border_array(test))
