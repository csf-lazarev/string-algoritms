def bs_to_bms(bs: list, n) -> list:
    bsm = [0 for _ in range(n)]
    bsm[0] = bs[0]
    bsm[-1] = 0
    for i in range(n - 2, 0, -1):
        # Проверка условия непродолжимости
        if bs[i] and bs[i] + 1 == bs[i - 1]:
            # Если грань оказалась продолжимой то мы берем значение уже посчитанное для подстроки длинны bp[i] из массива bmp -
            # по сути рассматривается только сама эта грань
            bsm[i] = bsm[n - bs[i]]
        else:
            bsm[i] = bs[i]
    return bsm


if __name__ == '__main__':
    s = ["A", "B", "X", "A", "B", "Z", "M", "A", "B", "X", "A", 'B', "Z"]
    bp = [6, 5, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0]
    print(bs_to_bms(bp, len(bp)))