def bmp_to_bp(bpm: list, n) -> list:
    bp = [0 for _ in range(n)]
    bp[n - 1] = bpm[n - 1]
    bp[0] = 0
    for i in range(n - 2, 0, -1):
        bp[i] = max(bp[i + 1] - 1, bpm[i])
    return bp


if __name__ == '__main__':
    bpm = [0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2, 6]
    print(bmp_to_bp(bpm, len(bpm)))
