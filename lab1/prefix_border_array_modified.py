def prefix_border_array_modified(s, bp):
    n = len(s)
    bpm = [0 for _ in range(len(s))]
    bpm[0] = 0
    bpm[-1] = bp[-1]
    for i in range(n):
        # Проверка условия непродолжимости
        if bp[i] != 0 and i < len(s) - 1 and s[bp[i]] == s[i + 1]:
            # Если грань оказалась продолжимой то мы берем значение уже посчитанное для подстроки длинны bp[i] из массива bmp -
            # по сути рассматривается только сама эта грань
            bpm[i] = bpm[bp[i] - 1]
        else:
            bpm[i] = bp[i]
    return bpm


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # s = ["c","a","c","z","z","z","c","a","c","a"]
    # bp = [0,0,1,0,0,0,1,2,3,2]
    s = ["A", "B", "X", "A", "B", "Z", "M", "A", "B", "X", "A", 'B', "Z"]
    bp = [0, 0, 0, 1, 2, 0, 0, 1, 2, 3, 4, 5, 6]
    print(s)
    print(bp)
    print(prefix_border_array_modified(s, bp))
