def bp_to_bmp(bp: list, n) -> list:
    bpm = [0 for _ in range(n)]
    bpm[-1] = bp[-1]
    for i in range(1, n - 1):
        # Проверка условия непродолжимости
        if bp[i] and bp[i] + 1 == bp[i + 1]:
            # Если грань оказалась продолжимой то мы берем значение уже посчитанное для подстроки длинны bp[i] из массива bmp -
            # по сути рассматривается только сама эта грань
            bpm[i] = bpm[bp[i] - 1]
        else:
            bpm[i] = bp[i]
    return bpm


if __name__ == '__main__':
    s = ["A", "B", "X", "A", "B", "Z", "M", "A", "B", "X", "A", 'B', "Z"]
    bp = [0, 0, 0, 1, 2, 0, 0, 1, 2, 3, 4, 5, 6]
    print(bp_to_bmp(bp, len(s)))
